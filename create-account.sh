#!/bin/bash

if ! [[ $(id -u) == 0 ]]; then
  echo 'Error: you need to run as root.'
  exit 1
fi

# create the kmoschkau account if it does not exist
getent passwd kmoschkau > /dev/null
if [ $? -ne 0 ]
then
    echo "Creating kmoschkau account ..."
	useradd -m -s /bin/bash kmoschkau
else
    echo "kmoschkau account exists - skipping creation"
fi

# add account to the sudoers file if it does not exist
cat /etc/sudoers|grep ^kmoschkau > /dev/null
if [ $? -ne 0 ]
then
    echo "Adding kmoschkau account to the sudoers file ..."
	echo "kmoschkau        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
	echo "Defaults:kmoschkau !requiretty" >> /etc/sudoers
else
    echo "kmoschkau account already in sudoers file - skipping"
fi

# configure the ssh pub key for login if it does not exist
if [ ! -s /home/kmoschkau/.ssh/authorized_keys ]
then
    if [ ! -d /home/kmoschkau/.ssh ]
	then 
		echo "Creating .ssh directory ..."
		mkdir /home/kmoschkau/.ssh
	else
	 	echo ".ssh directory already exists - skipping"
	fi
    
    # create the authorized key file for access
	# the public key configuration will allow ansible
	# to run in push mode if necessary.
    echo "Adding authorized_keys file ..."
	curl -o /home/kmoschkau/.ssh/authorized_keys https://gitlab.com/kmoschkau/pubssh/raw/master/id_rsa.pub
	chown kmoschkau:kmoschkau -R /home/kmoschkau/.ssh
	chmod 600 /home/kmoschkau/.ssh/authorized_keys
else
	echo "authorized_key file already exists - skipping"
fi

